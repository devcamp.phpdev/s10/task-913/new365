<!-- resources/views/home.blade.php -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container mt-5">
        <h2 class="mb-4">Tìm kiếm bài viết</h2>
        <form id="searchForm" class="form-inline">
            <div class="form-group mr-2">
                <label for="category_id" class="mr-2">ID Thể loại:</label>
                <input type="number" class="form-control" id="category_id" placeholder="Nhập ID thể loại">
            </div>
            <div class="form-group mr-2">
                <label for="keyword" class="mr-2">Từ khóa:</label>
                <input type="text" class="form-control" id="keyword" placeholder="Nhập từ khóa">
            </div>
            <button type="button" class="btn btn-primary" id="searchButton">Tìm kiếm</button>
        </form>
    </div>

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <!-- Custom Script -->
    <script>
        $(document).ready(function() {
            $('#searchButton').on('click', function() {
                var categoryId = $('#category_id').val();
                var keyword = $('#keyword').val();

                if (categoryId && keyword) {
                    var url = `/categories/${categoryId}/posts?keyword=${keyword}`;
                    window.location.href = url;
                } else {
                    alert('Vui lòng nhập cả ID thể loại và từ khóa.');
                }
            });
        });
    </script>
</body>
</html>
